mod matrix;
mod operations;
mod utils;

use std::io;
use operations::Ops;
use matrix::Matrix;

fn main() {
    let mut A: Matrix = Matrix::new(2, 2);
    let mut B: Matrix = Matrix::new(2, 2);
    let mut R: Matrix = Matrix::new(2, 2);
    loop {
        println!("*************\nMatix solver\nChose the operation to perform*************\n");
        println!("0) Fill Matrix A\n1) Fill Matrix B\n2) Show Matrix A\n3) Show Matrix B");
        println!("4) A + B\n5) A - B\n6) A * B\n7) A <--> B");
        println!("8) A^T\n9) B^T\n10) A^-1\n11) B^-1\n12) Ax = B");
        println!("13) R-->A\n14) R-->B\n15) Row Operations A\n16) Row Operations B\n17) Exit\n>> ");

        let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();
        input = input.trim().to_string();
        let option : i32 = input.parse().unwrap();
        let op = match option {
            0 => Ops::FillA,
            1 => Ops::FillB,
            2 => Ops::ShowA,
            3 => Ops::ShowB,
            4 => Ops::AplusB,
            5 => Ops::AminusB,
            6 => Ops::AtimesB,
            7 => Ops::Swap,
            8 => Ops::Atranspose,
            9 => Ops::Btranspose,
            10 => Ops::Ainverse,
            11 => Ops::Binverse,
            12 => Ops::Solution,
            13 => Ops::RtoA,
            14 => Ops::RtoB,
            15 => Ops::RowOperationsA,
            16 => Ops::RowOperationsB,
            _ => Ops::Exit
        };
        match op {
            Ops::FillA => {
                let (row, col) = utils::getRowCol("Write down the rows and columns of Matrix A");
                A = Matrix::new(row, col);
                A.fill_mat();
            },
            Ops::FillB => {
                let (row, col) = utils::getRowCol("Write down the rows and columns of Matrix B");
                B = Matrix::new(row, col);
                B.fill_mat();
            },
            Ops::ShowA => {
                A.print_mat("A");
            },
            Ops::ShowB => {
                B.print_mat("B");
            },
            Ops::AplusB => {
                R = A.add(&B);
                R.print_mat("R = A + B");
            },
            Ops::AminusB => {
                R = A.minus(&B);
                R.print_mat("R = A - B");
            },
            Ops::AtimesB => {
                R = A.multiply(&B);
                R.print_mat("R = A * B");
            },
            Ops::Swap => {
                let temp = A.copy();
                A = B.copy();
                B = temp.copy();
                println!("Swap operation succesully performed. A <--> B");
            },
            Ops::RtoA => {
                A = R.copy();
                println!("R --> A operation succesully performed.");
            },
            Ops::RtoB => {
                B = R.copy();
                println!("R --> B operation succesully performed.");
            },
            Ops::Atranspose => {
                R = A.transpose();
                R.print_mat("R = A^T");
            },
            Ops::Btranspose => {
                R = B.transpose();
                R.print_mat("R = B^T");
            },
            Ops::Ainverse => {
                if A.getRow() == A.getCol(){
                    R = A.inverse();
                    if R.getIsMatOk() {
                        R.print_mat("R = A^-1");
                    } else {
                        println!("Matrix inversion of A is not possible to perform! Possible causes:");
                        println!("* )Matrix has rows/columns not linearly independent.");
                    }
                    
                } else {
                    println!("Matrix inversion of A is not possible to perform! Possible causes:");
                    println!("* )Matrix has no square dimensions.");
                }
            },
            Ops::Binverse => {
                if B.getRow() == B.getCol(){
                    R = B.inverse();
                    if R.getIsMatOk() {
                        R.print_mat("R = B^-1");
                    } else {
                        println!("Matrix inversion of B is not possible to perform! Possible causes:");
                        println!("* )Matrix has rows/columns not linearly independent.");
                    }
                    
                } else {
                    println!("Matrix inversion of B is not possible to perform! Possible causes:");
                    println!("* )Matrix has no square dimensions.");
                }
            },
            Ops::Solution => {
                if A.getRow() == A.getCol() && B.getCol()==1 && B.getRow()==A.getCol(){
                    R = A.inverse();
                    if R.getIsMatOk() {
                        R = R.multiply(&B);
                        R.print_mat("R = x <==> Ax = B");
                    } else {
                        println!("Matrix inversion of A is not possible to perform! Possible causes:");
                        println!("* )Matrix has rows/columns not linearly independent.");
                    }
                    
                } else {
                    println!("Matrix solution of Ax = B is not possible to perform! Possible causes:");
                    println!("* )Matrix A has no square dimensions. nxn");
                    println!("* )Matrix B has no fitting dimensions. nx1");
                }
            },
            Ops::RowOperationsA => {
                let (row, col) = utils::getRowCol("Write down the row and column of A to be the pivot:");
                R = A.row_operations(row as usize,col as usize);
                R.print_mat("Pivot operations result over A");
            }
            Ops::RowOperationsB => {
                let (row, col) = utils::getRowCol("Write down the row and column of B to be the pivot:");
                R = B.row_operations(row as usize,col as usize);
                 R.print_mat("Pivot operations result over B");
            }
            _ => {
                println!("End of program." );
                break;
            }
        }
    }

}

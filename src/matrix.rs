use std::ops::{Index,IndexMut};
use std::io;
use crate::utils::extensions::{isZeroExt,
    multEscExt,sumVecEqExt
};

pub struct Matrix {
    row: usize,
    col: usize,
    isMatOk: bool,
    data: Vec<Vec<f64>>
}

impl Matrix {
    pub fn new(row: usize, col: usize) -> Self {
        let mut data = Vec::new();
        for _r in 0..row  {
            let mut temp_row = Vec::new();
            for _c in 0..col {
                let val = 0.0;
                temp_row.push(val);
            }
            data.push(temp_row);
        }
        Self {
            row,
            col,
            isMatOk: true,
            data
        }
    }
    pub fn getRow(&self) -> usize {
        self.row
    }
    pub fn getCol(&self) -> usize {
        self.col
    }
    pub fn getIsMatOk(&self) -> bool {
        self.isMatOk
    }
    pub fn copy(&self) -> Self {
        let mut data = Vec::new();
        let (row, col) = (self.row,self.col);
        for r in 0..row  {
            let mut temp_row = Vec::new();
            for c in 0..col {
                temp_row.push(self[r][c]);
            }
            data.push(temp_row);
        }
        Self {
            row,
            col,
            isMatOk: true,
            data
        }
    }
    
    pub fn print_mat(&self, name: &str){
        println!("-------- Matrix {} --------\n", name);
        for r in &self.data {
            for val in r  {
                print!("{0:4.4}\t", val );
            }
            println!();
        }
        println!("---------------------------");
    }

    pub fn fill_mat(&mut self){
        for r in 0..self.row {
            let mut data_row: Vec<f64> ;
            loop {
                let mut input = String::new();
                println!("Matrix row {}:",r);
                io::stdin().read_line(&mut input).unwrap();
                data_row = input
                    .split_whitespace()
                    .map(|s| s.parse().unwrap())
                    .collect();
                if data_row.len() == self.col {
                    break;
                } else {
                    println!("Data collected does not match Matrix columns ! Try again.");
                }
            }
            for c in 0..self.col {
                let val = data_row[c];
                self[r][c] = val;
            }
        }
    }
    pub fn add(&self, other: &Self) -> Self {
        let mut C = Matrix::new(self.row, self.col);
        for r in 0..C.row  {
            for c in 0..C.col  {
                C[r][c] = self[r][c] + other[r][c];
            }
        }
        C
    }
    pub fn minus(&self, other: &Self) -> Self {
        let mut C = Matrix::new(self.row, self.col);
        for r in 0..C.row  {
            for c in 0..C.col  {
                C[r][c] = self[r][c] - other[r][c];
            }
        }
        C
    }
    pub fn transpose(&self) -> Self {
        let mut C = Matrix::new(self.col, self.row);
        for r in 0..C.row  {
            for c in 0..C.col  {
                C[r][c] = self[c][r];
            }
        }
        C
    }
    pub fn multiply(&self, other: &Self) -> Self {
        let mut C = Matrix::new(self.row, other.col);
        let transp = other.transpose();
        for r in 0..C.row  {
            for c in 0..C.col  {
                for y in 0..self[r].len()  {
                    C[r][c] += self[r][y] * 
                        transp[c][y];
                }
            }
        }
        C
    }
    pub fn inverse(&self) -> Self {
        let mut AI = Matrix::new(self.row,self.col*2);
        let mut inv = Matrix::new(self.row,self.col);
        for i in 0..AI.row {
            for c in 0..AI.col {
                if c<self.col{ 
                    AI[i][c] = self[i][c];
                }
                else if c==(self.col+i){
                    AI[i][c] = 1.0;
                }
            }
        }
        for  r in 0..self.row {
            //pivot
            // isZero, multEsc, sumVecEq are Extension methods (trait implemented)
            if AI[r][r].isZero() {    
                let mut state = false;
                for i in (r+1)..self.row{
                    if !AI[i][r].isZero() {
                        let temp = AI[r].clone();
                        AI[r] = AI[i].clone();
                        AI[i] = temp.clone();
                        state = true;
                        break;
                    }
                }
                inv.isMatOk = state;
            }
            AI[r] = AI[r].clone().multEsc(1.0/AI[r][r]);
            for  c in 0..self.row {
                if r!=c {
                    AI[c] = AI[r].clone().multEsc(-1.0*AI[c][r])
                        .sumVecEq(AI[c].clone());
                }
            }
        }
        for r in 0..self.row{
            for c in self.row..self.row*2{
                inv[r][(c-self.row)] = AI[r][c];
            }
        }
        inv
    }
    pub fn row_operations(&self, row: usize, col: usize) -> Self {
        let mut mat = self.copy();
        let r = row;
        if mat[r][col].isZero(){
            println!("Pivot is zero, no row operations done.");
            return mat;
        }
        mat[r] = mat[r].clone().multEsc(1.0/mat[r][col]);
        for  c in 0..mat.row {
            let c = c;
            if r!= c {
                mat[c] = mat[r].clone().multEsc(-1.0*mat[c][col])
                    .sumVecEq(mat[c].clone());
            }
        }
        mat
    }
}

impl Index<usize> for Matrix {
    type Output = Vec<f64>;
    fn index(&self, index: usize) -> &Self::Output {
        &self.data[index]
    }
}

impl IndexMut<usize> for Matrix {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.data[index]
    }
}
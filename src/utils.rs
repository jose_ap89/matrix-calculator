use std::io;

pub fn getRowCol(message: &str) -> (usize,usize) {
    let row ;
    let col ;
    println!("{}:",message);
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let data : Vec<usize> = input.split_whitespace()
                        .map(|s| s.parse::<usize>().unwrap())
                        .collect();
    row = data[0];
    col = data[1];
    (row,col)
}

pub mod extensions {
    pub trait isZeroExt {
        fn isZero(&self) -> bool;
    }

    impl isZeroExt for f64 {
        fn isZero(&self) -> bool {
            self.abs() < 0.000000001
        }
    }
    pub trait multEscExt {
        fn multEsc(self, val: f64) -> Vec<f64>;
    }

    impl multEscExt for Vec<f64> {
        fn multEsc(self, val: f64) -> Vec<f64> {
            let mut res = vec![0.0;self.len()];
            for v in 0..res.len() {
                res[v] = self[v] * val;
            }
            res
        }
    }
    pub trait sumVecEqExt {
        fn sumVecEq(self, other: Vec<f64>) -> Vec<f64>;
    }

    impl sumVecEqExt for Vec<f64> {
        fn sumVecEq(self, other: Vec<f64>) -> Vec<f64> {
            let mut res = vec![0.0;self.len()];
            for v in 0..res.len() {
                res[v] = self[v] + other[v];
            }
            res
        }
    }
}